var express = require('express');
var app = express();
var scan = require('./scan');
var sysInfo = require('./utils/sys-info');

app.use(express.static('static'));

// const env = process.env;

app.get('/health', function(req,res) {
	res.send(200);
});

app.get('/info/:type', function(req,res) {
    res.haeder('Content-Type', 'application/json');
    res.header('Cache-Control', 'no-cache, no-store');
	res.send(JSON.stringify(sysInfo[req.params.type]()));
});

app.get('/scan/:location', function (req, res) {
	if (req.params.location) {
		const scanned = scan(...req.params.location.split(','));
		if(scanned) {
			scanned.then(data => res.send(JSON.stringify(data)))
		}
		else console.log('Wrong request: '+JSON.stringify(req.params));
	}

	else {
		res.send("No location found");
	}

});

app.listen(env.NODE_PORT || 3000, env.NODE_IP || 'localhost', function () {
    console.log(`Application worker ${process.pid} started...`);
});
